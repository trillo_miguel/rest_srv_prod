package com.techuniversity.prod.srvprod;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoParaGestionarProductosEnMongoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProyectoParaGestionarProductosEnMongoApplication.class, args);
    }

}
