package com.techuniversity.prod.srvprod;

import com.techuniversity.prod.srvprod.controllers.ProductoController;
import com.techuniversity.prod.srvprod.productos.ProductoModel;
import com.techuniversity.prod.srvprod.productos.ProductoRepository;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class ProductoRepoIntegrationTest {
    @Autowired
    ProductoRepository productoRepository;

    @Test
    public void testFindAll() {
        List<ProductoModel> productos = productoRepository.findAll();
        assertTrue(productos.size() > 0 );
    }

/*
    @LocalServerPort
    private int port;
    TestRestTemplate testRestTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();

    @Test
    public void testPrimerProducto() throws Exception {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = testRestTemplate.exchange(
               crearUrlConPuerto("/productos/productos/60acaa067803b53172947333"),
                HttpMethod.GET, entity, String.class
        );
        String expected = "{\"id\":\"60acaa067803b53172947333\"," + "\"nombre\":\"Producto 2\",\"descripcion\":\"Descripción producto 2\"," + "\"precio\": 23.6}";
        JSONAssert.assertEquals(expected, response.getBody(),false);
    }
    private String crearUrlConPuerto(String url) {
        return "http://localhost:" + port + url;
    }
*/
}
